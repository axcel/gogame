__all__ = [
    'Agent',
]


class Agent:
    def __init__(self):
        ...

    def select_move(self, game_state):
        raise NotImplementedError()