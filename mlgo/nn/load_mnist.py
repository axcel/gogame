import six.moves.cPickle as pickle
import gzip
import numpy as np


def encode_label(j: int):
    e = np.zeros((10, 1))
    e[j] = 1.0
    return e


def shape_date(data):
    features = [np.reshape(x, (784, 1)) for x in data[0]]
    labels = [encode_label(y) for y in data[1]]
    return list(zip(features, labels))


def load_data_impl():
    # file retrieved by:
    # wget https://github.com/mnielsen/neural-networks-and-deep-learning/raw/master/data/mnist.pkl.gz
    from pathlib import Path

    path = Path(__file__).parent.absolute()
    mnist_path = str(path) +'/mnist.pkl.gz'

    with gzip.open(mnist_path, 'rb') as f:
        train_data, _, test_data = pickle.load(f, encoding="latin1")
    return train_data, test_data


def load_data():
    train_data, test_data = load_data_impl()
    return shape_date(train_data), shape_date(test_data)
